import axios from 'axios'

let taleanconnector = {
  login: function (username, password) {
    let query = {
      'username': username,
      'password': password
    }
    return new Promise((resolve, reject) => {
      axios.post('http://localhost:3000/login', query)
        .then((response) => {
          resolve(response.data)
        })
        .catch((err) => {
          reject(err)
        })
    })
  },
  deleteresume: function (id) {
    return new Promise((resolve, reject) => {
      axios.get('http://localhost:3000/deleteresume?id=' + id)
        .then((response) => {
          resolve(response.data)
        })
        .catch((err) => {
          reject(err)
        })
    })
  },
  searchresumes: function (terms, from, size) {
    let ownerid = '5a78f90ae60dc618030708c3'

    let query = {
      'terms': terms
    }
    // console.log(query)
    return new Promise((resolve, reject) => {
      let urlquery = 'id=' + ownerid + '&from=' + from + '&size=' + size
      // console.log(urlquery)
      axios.post('http://localhost:3000/searchresumes?' + urlquery, query)
        .then((response) => {
          // console.log(response)
          if (response.status === 204) {
            resolve({
              'total': 0,
              'data': []
            })
          }
          else {
            let hits = response.data.results.hits.hits
            let total = response.data.results.hits.total
            let tdata = hits.map((val) => {
              let src = val._source
              src.id = val._id
              src.selected = false
              src.skills = val._source.skills.sort(function (a, b) {
                if (a.experience > b.experience) {
                  return -1
                }
                if (a.experience < b.experience) {
                  return 1
                }
                return 0
              })
              if (src.candidate.length > 30) {
                src.candidate = src.candidate.substring(0, 25)
              }
              let maxiter = 3
              if (src.skills.length < 3) {
                maxiter = src.skills.length
              }
              let topstr = ''
              for (let i = 0; i < maxiter; i++) {
                topstr += src.skills[i].name + ' | '
              }
              src.topstr = topstr.substring(0, topstr.length - 3)
              return src
            })
            let payload = {
              'total': total,
              'data': tdata
            }
            // console.log(response.data)
            resolve(payload)
          }
        })
        .catch((err) => {
          console.log(err)
          reject(err)
        })
    })
  },
  getjobmatches: function (userid, terms) {
    let body = {
      'terms': terms
    }
    return new Promise((resolve, reject) => {
      axios.post('http://localhost:3000/getjobmatches?id=' + userid, body)
        .then((response) => {
          if (response.status === 204) {
            resolve({
              'total': 0,
              'data': []
            })
          }
          else {
            let hits = response.data.results.hits.hits
            let total = response.data.results.hits.total
            let tdata = hits.map((val) => {
              let src = val._source
              src.id = val._id
              src.selected = false
              src.skills = val._source.skills.sort(function (a, b) {
                if (a.experience > b.experience) {
                  return -1
                }
                if (a.experience < b.experience) {
                  return 1
                }
                return 0
              })
              let maxiter = 3
              if (src.skills.length < 3) {
                maxiter = src.skills.length
              }
              let topstr = ''
              for (let i = 0; i < maxiter; i++) {
                topstr += src.skills[i].name + ' | '
              }
              src.topstr = topstr.substring(0, topstr.length - 3)
              return src
            })
            let payload = {
              'total': total,
              'data': tdata
            }
            // console.log(response.data)
            resolve(payload)
          }
        })
        .catch((err) => {
          reject(err)
        })
    })
  },
  searchskills: function (term) {
    return new Promise((resolve, reject) => {
      axios.get('http://localhost:3000/skill?name=' + term)
        .then((response) => {
          resolve(response.data)
        })
        .catch((err) => {
          reject(err)
        })
    })
  },
  getUserResumes: function (userid) {
    return new Promise((resolve, reject) => {
      axios.get('http://localhost:3000/resumes?id=' + userid)
        .then((response) => {
          resolve(response.data)
        })
        .catch((err) => {
          reject(err)
        })
    })
  },
  getUserJobs: function (userid) {
    return new Promise((resolve, reject) => {
      axios.get('http://localhost:3000/jobs?id=' + userid)
        .then((response) => {
          console.log(response)
          resolve(response.data)
        })
        .catch((err) => {
          reject(err)
        })
    })
  },
  deleteUserResumes: function (userid) {
    return new Promise((resolve, reject) => {
      axios.get('http://localhost:3000/deleteuserresumes?id=' + userid)
        .then((response) => {
          resolve(response.data)
        })
        .catch((err) => {
          reject(err)
        })
    })
  },
  deleteUserJobs: function (userid) {
    return new Promise((resolve, reject) => {
      axios.get('http://localhost:3000/deleteuserjobs?id=' + userid)
        .then((response) => {
          resolve(response.data)
        })
        .catch((err) => {
          reject(err)
        })
    })
  }
}

export default taleanconnector
