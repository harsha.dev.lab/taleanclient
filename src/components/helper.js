let helper = {
  containsCaseInsensitive: function (array, term) {
    for (let i = 0; i < array.length; i++) {
      if (array[i].toLowerCase() === term.toLowerCase()) {
        return true
      }
    }
    return false
  }
}

export default helper
